//SWAP TWO NUMBERS
#include<stdio.h>
void swap(int,int);
int main()
{
int a =10;
int b=20;
printf("before calling the function the value of a and b is=%d,%d\n",a,b);
swap(a,b);
printf("after calling the function the value of a and b is=%d,%d\n",a,b);
return 0;
}
void swap(int c,int d)
{
int temp;
temp=c;
c=d;
d=temp;
printf("the swapped value is=%d,%d\n",c,d);
}


//LARGEST OF THREE NUMBERS
#include<stdio.h>
int large(int,int,int);
int main()
{
int a,b,c,res;
printf("Enter the 3 numbers \n");
scanf("%d%d%d",&a,&b,&c);
res= large(a,b,c);
printf("the largest number is=%d\n",res);
}
int large(int a1,int b1,int c1)
{
if((a1>b1)&&(a1>c1))
{
return a1;
}
else if((b1>c1)&&(b1>a1))
{
return b1;
}
else
{
return c1;
}
}

//DISPLAY THE DAY OF THE WEEK
#include<stdio.h>
int main()

{
int i;
printf("Enter the number between 1-7\n");
scanf("%d",&i);
switch(i)
{
case 1:
printf("sunday\n");
break;
case 2:
printf("monday\n");
break;
case 3:
printf("tuesday\n");
break;
case 4:
printf("wednesday\n");
break;
case 5:
printf("thursday\n");
break;
case 6:
printf("friday\n");
break;
case 7:
printf("saturday\n");
break;
default:
printf("invalid input");
}
return 0;
}


// DISPLAY NUMBER IS ODD OR EVEN
#include<stdio.h>
int main()
{
int i;
printf("Enter the number between 1-10\n");
scanf("%d",&i);
switch(i)
{
case 1:
case 3:
case 5:
case 7:
case 9:
printf("It is an odd number\n");
break;
case 2:
case 4:
case 6:
case 8:
case 10:
printf("It is an even number\n");
break;

default:
printf("invalid input\n");
}
return 0;
}



