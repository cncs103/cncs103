//PROGRAM TO INSERT A NUMBER AT A GIVEN LOCATION IN THE ARRAY

#include<stdio.h>
#define SIZE 100
int main()    
{
    int arr[SIZE], i, n, ele, pos;
    printf("Enter size of the array :\n ");
    scanf("%d", &n);
    printf("Enter array elements:\n");
    for(i=0; i<n; i++)
    {
    scanf("%d", &arr[i]);
    
    }
    printf("Enter element to be insert :\n ");
    scanf("%d", &ele);
    printf("Enter position at which element to be insert:\n ");
    scanf("%d", &pos);
    for(i=n; i>pos; i--)
    {
    arr[i] = arr[i-1];
    }
    arr[pos] = ele;
    n=n+1;
    printf("Array elements after insertion :\n "); 
    for(i=0; i<n+1; i++)
    {
     printf("%d\n", arr[i]);
    }
    return 0;
    
}