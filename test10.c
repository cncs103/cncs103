//A PROGRAM TO COPY CONTENT OF ONE FILE TO ANOTHER USING FILE OPERATIONS

#include<stdio.h>
#include <conio.h>
#include<stdlib.h>
#include<process.h>
void main()
{
  FILE *fs, *ft;
  char ch;
  fs=fopen("File1.txt","r");
  if(fs==NULL)
  {
       puts("cannot open the file");
       exit(0);
  }
 ft=fopen("File2.txt","w");
while(1)
{
 ch=fgetc(fs);
  if(ch==EOF)
   break;
else
   fputc(ch,ft);
}
fclose(fs);
fclose(ft);
}
