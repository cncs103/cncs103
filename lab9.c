//A PROGRAM TO ENTER A CHARACTER AND DETERMINE IF IT IS A VOWEL OR NOT USING POINTERS

#include <stdio.h>
int main()
{
    char str[100];
    char *ptr;
    ptr=str;
    printf("Enter a character: ");
    scanf("%c",ptr);
    if(*ptr=='A' ||*ptr=='E' ||*ptr=='I' ||*ptr=='O' ||*ptr=='U' ||*ptr=='a' ||*ptr=='e' ||*ptr=='i' ||*ptr=='o' ||*ptr=='u')
    printf("IT IS A VOWEL");  
    else
    printf("IT IS NOT A VOWEL");
    return 0;
}


//A PROGRAM TO SWAP TWO NUMBERS USING POINTERS

#include <stdio.h>
 int main()
{
   int x, y, *a, *b, temp;
   printf("Enter the value of x and y\n");
   scanf("%d%d", &x, &y);
   printf("Before Swapping\nx = %d\ny = %d\n", x, y);
   a = &x;
   b = &y;
   temp = *b;
   *b = *a;
   *a = temp;
   printf("After Swapping\nx = %d\ny = %d\n", x, y);
   return 0;
}
