//SUM OF FIRST 10 NATURAL NUMBERS
#include<stdio.h>
 int main()
 {
 int i=1,sum=0;
 while(i<=10)
 {
 sum=sum+i;
 i=i+1;
 }
 printf("The sum of first 10 natural numbers is=%d\n",sum);
 return 0;
 }

//ODD NUMBERS
#include<stdio.h>
 int main()
 {
 int i=1;
  printf("The odd numbers between 1 and 20 are\n");
  for(i=1;i<=20;i+=2)
 printf("%d\n",i);
 return 0;
}

//REVERSE OF A 3 DIGIT NUMBER
 #include<stdio.h>
 int main()
 {
 int n,temp=0;
 printf("Enter a 3 digit number\n");
 scanf("%d",&n);
 printf("The reverse number is=");
 while(n!=0)
 {
	 temp=n%10;
	 printf("%d",temp);
	 n=n/10;
 }
 return 0;
}


//MULTIPLICATION TABLE OF 12
 #include<stdio.h>
 int main()
 {
 int i,p;
 printf("The multiplication table of 12 is\n");
 for(i=1;i<=10;i++)
 {
 p=12*i;
 printf("12*%d=%d\n",i,p);
 }
 return 0;
}

