//PROGRAM TO READ THE MARKS OF 5 STUDENTS IN 3 SUBJECTS USING ARRAY AND DISPLAY THE HIGHEST MARKS

#include<stdio.h>
int main()
{
    int marks[50][50],stu,subj,highest_marks1,highest_marks2,highest_marks3;
    for(stu=0;stu<5;stu++)
      {
        for(subj=0;subj<3;subj++)
          {
            printf("enter marks for student %d in subject %d =",stu+1,subj+1);
            scanf("%d",&marks[stu][subj]);
          }
      }
    printf("marks list -\n");  
    for(stu=0;stu<5;stu++)
      {
        for(subj=0;subj<3;subj++)
          {
            printf("%d\t",marks[stu][subj]);
          }
        printf("\n");  
      } 
    for(subj=0;subj<3;subj++)
      {
          highest_marks1=marks[0][0];
          highest_marks2=marks[0][1];
          highest_marks3=marks[0][2];
          for(stu=0;stu<5;stu++)
            {
                if(marks[stu][0]>highest_marks1)
                 {
                    highest_marks1=marks[stu][0]; 
                 }
                if(marks[stu][1]>highest_marks2)
                 {
                    highest_marks2=marks[stu][1]; 
                 }
                if(marks[stu][2]>highest_marks3)
                 {
                     highest_marks3=marks[stu][2];
                 }
            }
      }
    printf("\nhighest marks in subject 1 = %d",highest_marks1);  
    printf("\nhighest marks in subject 2 = %d",highest_marks2);  
    printf("\nhighest marks in subject 3 = %d",highest_marks3);  
    return 0;
}
