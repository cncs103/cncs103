1//FACTORIAL OF A NUMBER
#include<stdio.h>
int main()
{
	int i,fact=1,n;
	printf("Enter a number\n");
	scanf("%d",&n);
	if(n==0)
	{
		fact=1;
	
	}
	else
	{
		for(i=1;i<=n;i++)
		fact=fact*i;
	}
	printf("The factorial of the number is = %d\n",fact);
	return 0;
	}
	
	
2//REVERSE OF A 3 DIGIT NUMBER
	#include<stdio.h>
void main()
{
    int n,dig,rev=0;
    printf("Enter a number:\n");
    scanf("%d",&n);
    while(n!=0)
    {
        dig=n%10;
        rev=rev*10+dig;
        n=n/10;
    }
    printf("Reverse of the number is : %d",rev);
}


3//SUM OF THE SERIES 
#include<stdio.h>
#include<math.h>
void main()
{
    int i,n;
    float sum=0.0;
    printf("Enter the value of n : \n");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        sum=sum+(1/pow(i,2));
    }
    printf("Sum of the series is : %f",sum);
}

4//PATTERN 1
#include<stdio.h>
int main()
{
	int i,j;
	for(i=1;i<=5;i++)
	{
		printf("\n");
		for(j=1;j<=i;j++)
		printf("%d",i);
	}
	return 0;
}

5//PATTERN 2
#include<stdio.h>
int main()
{
	int i,j;
	for(i=65;i<=70;i++)
	{
		printf("\n");
		for(j=65;j<=i;j++)
		printf("%c",j);
	}
	return 0;
}

6//GCD OF TWO NUMBERS
#include<stdio.h>
int main()
{
	int num1,num2,divisor,dividend,remainder;
	printf("Enter two numbers\n");
	scanf("%d %d",&num1,&num2);
	if(num1>num2)
	{
		dividend = num1;
		divisor = num2;
		
	}
	else
	{
		dividend = num2;
		divisor = num1;
	}
	while(divisor)
	{
		remainder = dividend%divisor;
		dividend = divisor;
		divisor = remainder;
	}
	printf("The GCD of two numbers %d and %d is = %d\n",num1,num2,dividend);
	return 0;
}

--------------------------------------------------------------------------------------------------------------------