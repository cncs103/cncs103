//A PROGRAM TO DEMONSTRATE fgetc() AND fputc() FUNCTION

#include<stdio.h> 
#include<conio.h>
int main() 
{  
FILE *fl; 
char c;
printf("Data input:\n"); 
fl =fopen("Input.txt", "w");  //Open the file Input
while((c=getchar())!=EOF)    //get a character from key board
{
  fputc( c, fl);       //write a character to Input file
}
fclose(fl);            //close the file input
printf("Data output:\n"); 
fl =fopen("Input.txt" ,"r"); //Reopen the file input
while((c=fgetc(fl))!=EOF)    //reads each character from Input file
{ 
  printf("%c",c);
}
fclose(fl);  
}