//A PROGRAM TO READ AND DISPLAY INFORMATION OF THE STUDENT USING STRUCTURES

#include<stdio.h>
int main()
{
struct DOB{
int day;
int month;
int year;
};
struct student
{
int roll_no;
char name[100];
float fees;
struct DOB date;
};
struct student stud1;
printf("Enter the roll number:\n");
scanf("%d",&stud1.roll_no);
printf("Enter the name:\n");
scanf("%s",stud1.name);
printf("\n Enter the fees:\n");
scanf("%f",&stud1.fees);
printf("\n Enter the DOB:\n");
scanf("%d%d%d",&stud1.date.day,&stud1.date.month,&stud1.date.year);
printf("\n ***** STUDENT'S DETAILS ********");
printf("\nROLL NO=%d",stud1.roll_no);
printf("\nNAME =%s",stud1.name);
printf("\n FEES=%f",stud1.fees);
printf("\n DOB=%d-%d-%d",stud1.date.day,stud1.date.month,stud1.date.year);
getch();
return 0;
}



